package br.ucsal.bes20202.testequalidade.locadora;


import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Modelo;

/**
 * Testes UNITÃ�RIOS para os mÃ©todos da classe LocacaoBO (utilize o MOCKITO).
 * 
 * @author claudioneiva
 *
 */

public class LocacaoBOTest {

	/**
	 * Testar o cÃ¡lculo do valor de locaÃ§Ã£o por 3 dias para 2 veÃ­culos com 1 ano de
	 * fabricaÃ§ao e 3 veÃ­culos com 8 anos de fabricaÃ§Ã£o.
	 */
	
	private static final String DEFAULT_PLACA = "NYY8634";
	private static final Integer DEFAULT_ANO = 2012;
	private static final Modelo DEFAULT_MODELO = new Modelo("Fiat Uno");
	private static final Double DEFAULT_VALOR_DIARIA = 50.00;
	private static final SituacaoVeiculoEnum DEFAULT_SITUACAO = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = DEFAULT_PLACA;
	private Integer ano = DEFAULT_ANO;
	private Modelo modelo = DEFAULT_MODELO;
	private Double valorDiaria = DEFAULT_VALOR_DIARIA;
	private SituacaoVeiculoEnum situacao = DEFAULT_SITUACAO;
	
	private LocacaoBOTest() {}
	
	public LocacaoBOTest aCarro() {
		return new LocacaoBOTest();
	}
	
	public LocacaoBOTest withPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public LocacaoBOTest fromAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public LocacaoBOTest fromModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public LocacaoBOTest withValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public LocacaoBOTest withSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public LocacaoBOTest but() {
		return aCarro().withPlaca(placa).fromAno(ano).fromModelo(modelo).withValorDiaria(valorDiaria).withSituacao(situacao);
	}
	
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(ano);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return ;	
	}

	public Veiculo build() {
		// TODO Auto-generated method stub
		return null;
	}

}
