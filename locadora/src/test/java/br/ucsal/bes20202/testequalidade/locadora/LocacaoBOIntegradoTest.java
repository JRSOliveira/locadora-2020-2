package br.ucsal.bes20202.testequalidade.locadora;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Testes INTEGRADOS para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
@Test
public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		LocacaoBOTest LocacaoBOTest = LocacaoBOTest.aCarro();
		
		Veiculo carro1 = LocacaoBOTest(); //Utilizando as informa��es DEFAULT para montar o primeiro carro
		Veiculo carro2 = LocacaoBOTest.but().fromModelo(new Modelo("Chevrolet Classic")).withValorDiaria(65.65).build();  //Aproveitando o ano padr�o de 2012
		Veiculo carro3 = LocacaoBOTest.but().fromModelo(new Modelo("Fiat Uno")).withValorDiaria(40.48).build(); //Aproveitando o ano padr�o de 2012
		Veiculo carro4 = LocacaoBOTest.but().fromModelo(new Modelo("Ford Fusion")).fromAno(2019).withValorDiaria(75.43).build(); //Alterando o ano para 2019
		Veiculo carro5 = LocacaoBOTest.but().fromModelo(new Modelo("Fiat Toro")).fromAno(2019).withValorDiaria(91.00).build(); //Alterando o ano para 2019
		
		//Criando lista de carros para o teste
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		veiculos.add(carro1);
		veiculos.add(carro2);
		veiculos.add(carro3);
		veiculos.add(carro4);
		veiculos.add(carro5);
		
		Double expectedResult = 920.841;
		Double actualResult = LocacaoBO.calcularValorTotalLocacao(veiculos, 3);
		
		Assertions.assertEquals(expectedResult, actualResult);
		
	}

	private Veiculo LocacaoBOTest() {
		// TODO Auto-generated method stub
		return null;
	}
		
	}


